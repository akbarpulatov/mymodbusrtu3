#ifndef __TIMERS_H
#define __TIMERS_H


void SYSCLK_Init(void);
void Timer2_Init(void);
void Timer_4_Init(void);
void DelayNop(void);
unsigned short int CRC16(unsigned char *p, unsigned short int len);

#endif // !__TIMERS_H
