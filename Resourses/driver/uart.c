/********************************************************************************
 * project																		*
 *																				*
 * file			uart.c															*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *																				*
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "uart.h"
#include "RingBuffer.h"
#include "mydeff.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/

volatile uint8_t uartTxBuffer[UART_TX_BUFFER_SIZE] = { 0x11, 0x03, 0x04, 0x00, 0x00, 0x00, 0xF0, 0xEB, 0xB6};
volatile uint8_t uartRxBuffer[UART_RX_BUFFER_SIZE];
volatile uint8_t uartBufferIndex = 0;


/********************************************************************************
 * Uart Initialization function
 ********************************************************************************/

void UART_Init(USART_TypeDef* UART)
{
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN
				 |  RCC_APB2ENR_IOPAEN;
	
	//PA9 Tx AFPP
	GPIOA->CRH &= ~(GPIO_CRH_CNF9 | GPIO_CRH_MODE9_Msk);
	GPIOA->CRH |= (0b11 << GPIO_CRH_MODE9_Pos) | (0b10 << GPIO_CRH_CNF9_Pos);
	
	//PA10 Rx INF/IPUP
	GPIOA->CRH &= ~(GPIO_CRH_CNF10 | GPIO_CRH_MODE10_Msk);
	GPIOA->CRH |= (0b00 << GPIO_CRH_MODE10_Pos) | (0b10 << GPIO_CRH_CNF10_Pos);
	GPIOA->BSRR = GPIO_BSRR_BS10;
	
	
	//UART CONFIGURATION
	UART->CR1 |= USART_CR1_UE;
	UART->CR1 |= 0 << USART_CR1_M_Pos;
	UART->CR2 |= 0b00 << USART_CR2_STOP_Pos;
	UART->CR3 |= 1 << USART_CR3_DMAT_Pos;
	
	//CONFIGURE THE DMA REGISTERS
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	
	DMA_UART->CCR &= ~(DMA_CCR_EN);
	DMA_UART->CPAR = (uint32_t)(&(UART->DR));
	DMA_UART->CCR |= (0b10 << DMA_CCR_PL_Pos);
	DMA_UART->CCR |= (1 << DMA_CCR_DIR_Pos)
				  |  (0 << DMA_CCR_CIRC_Pos)
				  |  (0 << DMA_CCR_PINC_Pos)
				  |  (1 << DMA_CCR_MINC_Pos)
				  |  (0b10 << DMA_CCR_PSIZE_Pos)
				  |  (0b00 << DMA_CCR_MSIZE_Pos)
				  |  (1 << DMA_CCR_TCIE_Pos)
				  |  (0 << DMA_CCR_TEIE_Pos);
	NVIC_EnableIRQ(DMA1_Channel4_IRQn);
	
	//BAUD RATE = 470588 only for debug purposes, can be used 0x04 | 0x0E but the baud rate is twice
	UART->BRR &= ~(USART_BRR_DIV_Fraction | USART_BRR_DIV_Mantissa);
	//	UART->BRR |= (0x009 << USART_BRR_DIV_Mantissa_Pos) | (0xC << USART_BRR_DIV_Fraction_Pos);
	UART->BRR = 3750;
	UART->CR1 |= USART_CR1_TE | USART_CR1_RE;
	USART1->CR1 |= USART_CR1_RXNEIE;
	NVIC_EnableIRQ(USART1_IRQn);
	
	Uart_Send_Dma((uint8_t*)"\rProgram Begin\r", 15);
}

/********************************************************************************
 * Uart Interrupt Handler
 ********************************************************************************/

void USART1_IRQHandler(void)
{

	//Set Downcounter
	TIM2->CNT = 1000 - 1;
	
	//Counter Enable
	TIM2->CR1 |= TIM_CR1_CEN;
	
	//Reading Uart RX
	if (USART1->SR & USART_SR_RXNE)
	{
		uartRxBuffer[uartBufferIndex++] = (uint8_t)USART1->DR;
	}
	
	else if (USART1->SR & USART_SR_TXE)
		;
}
//------------------------------------------------------
void Uart_Send_Dma(uint8_t *data, int32_t len)
{
	DMA_UART->CCR &= ~(DMA_CCR_EN);
	DMA_UART->CMAR = (uint32_t)data;
	DMA_UART->CNDTR = len;
	DMA_UART->CCR |= DMA_CCR_EN;
}
//------------------------------------------------------
void Uart_Send(USART_TypeDef* Uart, uint8_t* data, uint16_t len) 
{
	
}
//------------------------------------------------------
void Uart_Receive(USART_TypeDef* Uart, uint8_t* data, uint16_t len)
{
	
}
//------------------------------------------------------
void DMA_Init(DMA_Channel_TypeDef* DMA_Channel)
{
	
}
//------------------------------------------------------
void DMA1_Channel4_IRQHandler(void)
{
	//bp;
	if(DMA1->ISR & DMA_ISR_TCIF4)
	{
		DMA1->IFCR |= DMA_IFCR_CGIF4;
		//Disable the DMA
		DMA_UART->CCR &= ~(DMA_CCR_EN);
		DIR485 = 0;
		DEB = 0;
	}
}


/********************************* END OF FILE **********************************/