/********************************************************************************
 * project																		*
 *																				*
 * file			spi.c															*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *																				*
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/

#include "spi.h"
#include "mydeff.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/


/********************************************************************************
 * SPI Initialization Function
 ********************************************************************************/

void SpiInit(SPI_TypeDef* SPIx)
{
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN
				 |  RCC_APB2ENR_SPI1EN
				 |  RCC_APB2ENR_IOPAEN;
	
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	
	//PA4 GPIO OUTPUT push pull for SPI CS
	GPIOA->ODR |= GPIO_ODR_ODR4;
	GPIOA->CRL &= ~(GPIO_CRL_CNF4_Msk | GPIO_CRL_MODE4_Msk);
	GPIOA->CRL |= (0b11 << GPIO_CRL_MODE4_Pos) | (0b00 << GPIO_CRL_CNF4_Pos);
	
	//SCK = PA5
	GPIOA->CRL &= ~(GPIO_CRL_CNF5_Msk | GPIO_CRL_MODE5_Msk);
	GPIOA->CRL |= (0b11 << GPIO_CRL_MODE5_Pos) | (0b10 << GPIO_CRL_CNF5_Pos);
	
	//MISO = PA6
	GPIOA->CRL &= ~(GPIO_CRL_CNF6_Msk | GPIO_CRL_MODE6_Msk);
	GPIOA->CRL |= (0b00 << GPIO_CRL_MODE6_Pos) | (0b01 << GPIO_CRL_CNF6_Pos);
	GPIOA->ODR |= (GPIO_ODR_ODR6);
	
	//MOSI = PA7
	GPIOA->CRL &= ~(GPIO_CRL_CNF7 | GPIO_CRL_MODE7_Msk);
	GPIOA->CRL |= (0b11 << GPIO_CRL_MODE7_Pos) | (0b10 << GPIO_CRL_CNF7_Pos);
	
	//SPI CONFIGURATION
	SPIx->CR1 |= (0b000 << SPI_CR1_BR_Pos)
			  |  (0 << SPI_CR1_CPOL_Pos)
			  |  (0 << SPI_CR1_CPHA_Pos)
			  |  (0 << SPI_CR1_DFF_Pos)
			  |  (0 << SPI_CR1_LSBFIRST_Pos)
			  |  (1 << SPI_CR1_SSM_Pos)
			  |  (1 << SPI_CR1_SSI_Pos)
			  |  (1 << SPI_CR1_MSTR_Pos);
	//SPI Enable
	SPIx->CR1 |= (1 << SPI_CR1_SPE_Pos);
	NVIC_EnableIRQ(SPI1_IRQn);
}

/********************************************************************************
 * SPI 
 ********************************************************************************/

uint8_t SPIx_WriteRead(uint8_t data)
{
	//wait for set Txe flag
	while(!(SPI_165->SR & SPI_SR_TXE));
	//CS_LOW   
	
	//send data
	SPI_165->DR = data;
	
	//wait for the responce
	while(!(SPI_165->SR & SPI_SR_RXNE));
	//read the received data
	data = SPI_165->DR;
	//CSHIGH;
	
    //return the received data
	return data;  
}

void SPI_SendByte(uint8_t data)
{
	SPIx_WriteRead(data);
}

uint8_t SPI_ReceiveByte(void)
{
	return SPIx_WriteRead(0x00);
}

/********************************* END OF FILE **********************************/