/********************************************************************************
 * project																		*
 *																				*
 * file			GPIO.c															*
 * author		Akbar Pulatov													*
 * date																			*
 * copyright	Akbar Pulatov(C)												*
 * brief																		*
 *																				*
 ********************************************************************************/

/********************************************************************************
 * Include 
 ********************************************************************************/
#include "stm32f1xx.h"
#include "GPIO.h"
#include "mydeff.h"
#include "spi.h"
#include "Timers.h"

/********************************************************************************
 * Used variable
 ********************************************************************************/

Word spiWord[2];
uint8_t spiByte[4] = { 0xFF, 0xFF, 0xFF, 0xFF };
extern unsigned short   usRegInputStart;
extern unsigned short   usRegInputBuf[REG_INPUT_NREGS];

/********************************************************************************
 * GPIO Initialization Function
 ********************************************************************************/

void GPIO_Init(void)
{
	RCC->APB2ENR |=  RCC_APB2ENR_IOPCEN
				 |	 RCC_APB2ENR_IOPAEN;
	
	//PC13
	GPIOC->CRH &= ~(GPIO_CRH_CNF13_Msk | GPIO_CRH_MODE13_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE13_Pos) | (0b00 << GPIO_CRH_CNF13_Pos);
	
	//PC14
	GPIOC->CRH &= ~(GPIO_CRH_CNF14_Msk | GPIO_CRH_MODE14_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE14_Pos) | (0b00 << GPIO_CRH_CNF14_Pos);
	
	//PA11
	GPIOA->CRH &= ~(GPIO_CRH_CNF11_Msk | GPIO_CRH_MODE11_Msk);
	GPIOA->CRH |= (0b11 << GPIO_CRH_MODE11_Pos) | (0b00 << GPIO_CRH_CNF11_Pos);
	
	//PC15
	GPIOC->CRH &= ~(GPIO_CRH_CNF15_Msk | GPIO_CRH_MODE15_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE15_Pos) | (0b00 << GPIO_CRH_CNF15_Pos);
	
	//PA8 
	GPIOA->CRH &= ~(GPIO_CRH_CNF8 | GPIO_CRH_MODE8_Msk);
	GPIOA->CRH |= (0b11 << GPIO_CRH_MODE8_Pos) | (0b00 << GPIO_CRH_CNF8_Pos);
	
	
	
//	//PA4 
//	GPIOA->CRL &= ~(GPIO_CRL_CNF4 | GPIO_CRL_MODE4_Msk);
//	GPIOA->CRL |= (0b11 << GPIO_CRL_MODE4_Pos) | (0b00 << GPIO_CRL_CNF4_Pos);
//	
//	//PA5 
//	GPIOA->CRL &= ~(GPIO_CRL_CNF5 | GPIO_CRL_MODE5_Msk);
//	GPIOA->CRL |= (0b11 << GPIO_CRL_MODE5_Pos) | (0b00 << GPIO_CRL_CNF5_Pos);
//	
//	//PA6 
//	GPIOA->CRL &= ~(GPIO_CRL_CNF6 | GPIO_CRL_MODE6_Msk);
//	GPIOA->CRL |= (0b00 << GPIO_CRL_MODE6_Pos) | (0b01 << GPIO_CRL_CNF6_Pos);
	
	
	
	
//	//PA2 for External Interrupt
//	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN
//				 |  RCC_APB2ENR_AFIOEN;
//	//PA2
//	GPIOA->CRL &= ~(GPIO_CRL_CNF2_Msk | GPIO_CRL_MODE2_Msk);
//	GPIOA->CRL |= (0b00 << GPIO_CRL_MODE2_Pos) | (0b10 << GPIO_CRL_CNF2_Pos);
//	GPIOA->ODR |= 1 << GPIO_ODR_ODR2_Pos;
//	
//	//EXTI2 for PA
//	AFIO->EXTICR[0] |= AFIO_EXTICR1_EXTI2_PA;
//	//Falling Edge
//	EXTI->FTSR |= EXTI_FTSR_TR2;
//	//Setting Up Mask
//	EXTI->IMR |= EXTI_IMR_MR2;
//	//Permit Interrupt On Line EXTI2
//	NVIC_EnableIRQ(EXTI2_IRQn);
	
}

/********************************************************************************
 * Updates the inputs
 ********************************************************************************/

void UpdateInputs(void)
{
	Ser_ST = 0;
	Ser_ST = 1;
	int trtrt = 0;
//	for (BYTE i = 0; i < 3; i++)
//	{
	spiWord[0].v[0] = SPI_ReceiveByte();
	spiWord[0].v[1] = SPI_ReceiveByte();
	spiWord[1].v[0] = SPI_ReceiveByte();
	spiWord[1].v[1] = SPI_ReceiveByte();
		//spiByte[i] = SPI_ReceiveByte();
//	}
}

/********************************************************************************
 * Updates Registers
 ********************************************************************************/

void UpdateRegisters(void)
{
	Byte tmp0;
	Byte tmp1;
	Byte tmp2;
	Byte tmp3;
	
	tmp0._byte = ~spiByte[0];
	tmp1._byte = ~spiByte[1];
	tmp2._byte = ~spiByte[2];
	tmp3._byte = ~spiByte[3];
		
	usRegInputBuf[0] = (tmp1._byte << 8) | tmp2._byte;
	usRegInputBuf[1] = (tmp3._byte << 8) | tmp0._byte;
}

uint32_t getIn165(void)
{
	uint32_t temp165 = 0;
	uint32_t temp = 0;
	uint8_t  DATA_WIDTH = 20;
	
	CP_165 = 1;
	PL_165 = 0;	
	DelayNop();
	
	PL_165 = 1;
	CP_165 = 0;
	DelayNop();
	
	for (int i = 0; i <= DATA_WIDTH; i++)
	{
		if (Q7_165 == 1)
		{
			temp = 1;
		}
		else
		{
			temp = 0;
		}

		temp165 |= (temp << (DATA_WIDTH - i));

		CP_165 = 1;
		DelayNop();
		CP_165 = 0;
		DelayNop();
	}
	return temp165;
}

/********************************* END OF FILE **********************************/
