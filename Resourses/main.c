#include "stm32f1xx.h"
#include "Timers.h"
#include "uart.h"
#include "GPIO.h"
#include "mydeff.h"
#include "spi.h"


void SetInterPriorities(void);
void DelayNop(void);

extern uint8_t spiByte[4];



unsigned short   usRegInputStart = REG_INPUT_START;
unsigned short   usRegInputBuf[REG_INPUT_NREGS] = { 0 };

int main(void)
{
	SYSCLK_Init();
	GPIO_Init();
	Timer2_Init();
	Timer_4_Init();
	UART_Init(USART1);
	SpiInit(SPI1);
	
	SetInterPriorities();
	
	while (1)
	{
		delaypolling(200000);
		LEDG ^= 1;
	}
}

void SetInterPriorities(void)
{
	NVIC_SetPriorityGrouping(0b010);
	
	// UART1
	NVIC_SetPriority(USART1_IRQn, 0);
	NVIC_SetPriority(TIM2_IRQn, 2);
	//	NVIC_SetPriority(TIM4_IRQn, 3);
	NVIC_SetPriority(DMA1_Channel4_IRQn, 3);
	
	
	// NVIC_SetPriority(USART1_IRQn, 0);
	
	
}












